import axios from '../http/http-common';

const URL = `/todos`;

const getTodos = (params) => {
  return axios.get(URL, { params });
};

const addTodo = (data) => {
  return axios.post(URL, data);
};

const updateTodo = (id, data) => {
  const url = URL + `/${id}`;
  return axios.patch(url, data);
};

const deleteTodo = (id) => {
  const url = URL + `/${id}`;
  return axios.delete(url);
};

export default {
  getTodos,
  addTodo,
  updateTodo,
  deleteTodo,
};
