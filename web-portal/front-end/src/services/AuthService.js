import axios from '../http/http-common';

const URL = `/accounts`;

const login = (params) => {
  return axios.get(URL, { params }).then((res) => {
    if (res) {
      localStorage.setItem('account', JSON.stringify(res.data));
    }
    return res;
  });
};

const filterCheckAccount = (params) => {
  if (params) {
    return axios.get(URL, { params });
  }
  return { error: 'error' };
};

const register = (data) => {
  return axios.post(URL, data);
};

const logout = () => {
  localStorage.removeItem('account');
};

const getCurrentAccount = () => {
  return JSON.parse(localStorage.getItem('account'));
};

export default {
  register,
  login,
  logout,
  getCurrentAccount,
  filterCheckAccount,
};
