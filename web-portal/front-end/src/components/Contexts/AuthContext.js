import { createContext } from 'react';
import AuthService from '../../services/AuthService';

export const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  const checkRegiters = async (account) => {
    const params = { email: account.email };
    return AuthService.filterCheckAccount(params).then((res) => {
      const acc = res.data;
      if (Object.keys(acc).length > 0) {
        return (res = { message: 'Account is exists!' });
      }
      return (res = { code: 200 });
    });
  };

  const authContextData = { checkRegiters };
  return <AuthContext.Provider value={authContextData}>{children}</AuthContext.Provider>;
};
