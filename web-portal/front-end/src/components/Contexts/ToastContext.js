import { createContext } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export const ToastContext = createContext();

export const ToastContextProvider = ({ children }) => {
  const showMessage = (type, title) => {
    const time = { autoClose: 3000 };
    const position = { position: toast.POSITION.BOTTOM_RIGHT };

    switch (type) {
      case 'success':
        return toast.success(title, position, time);
      case 'error':
        return toast.error(title, position, time);
      case 'info':
        return toast.info(title, position, time);
      case 'warning':
        return toast.warning(title, position, time);
      default:
        return;
    }
  };

  const toastContextData = { showMessage };
  return <ToastContext.Provider value={toastContextData}>{children}</ToastContext.Provider>;
};
