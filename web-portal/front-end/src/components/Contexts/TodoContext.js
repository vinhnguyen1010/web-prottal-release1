import { createContext, useReducer } from 'react';
import { ADD_TODO, DELETE_TODO, GET_TODOS, UPDATE_TODO } from '../../reducers';
import { todoReducers } from '../../reducers/TodoReducer';
import TodoService from '../../services/TodoService';

export const TodoContext = createContext();

export const TodoContextProvider = ({ children }) => {
  const [todos, dispatch] = useReducer(todoReducers, []);

  const createTodo = async (todo) => {
    return TodoService.addTodo(todo).then((res) => {
      dispatch({
        type: ADD_TODO,
        payload: res.data,
      });
      return res;
    });
  };

  const fetchTodos = async (params) => {
    return TodoService.getTodos(params).then((res) => {
      dispatch({
        type: GET_TODOS,
        payload: res.data,
      });
      return res;
    });
  };

  const editTodo = async (id, todo) => {
    return TodoService.updateTodo(id, todo).then((res) => {
      dispatch({
        type: UPDATE_TODO,
        payload: res.data,
      });
      return res;
    });
  };

  const removeTodo = async (todo) => {
    return TodoService.deleteTodo(todo.id).then((res) => {
      dispatch({
        type: DELETE_TODO,
        payload: todo,
      });
      return res;
    });
  };

  const todoContextData = { todos, fetchTodos, createTodo, editTodo, removeTodo, dispatch };

  return <TodoContext.Provider value={todoContextData}>{children}</TodoContext.Provider>;
};
