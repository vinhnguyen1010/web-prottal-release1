import React from 'react';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './toast.scss';

const Toasts = () => {
  return (
    <>
      <div className="notification-toast">
        <p className="notification-title">Title</p>
        <p className="notification-message">Message</p>
      </div>
    </>
  );
};
export default Toasts;
