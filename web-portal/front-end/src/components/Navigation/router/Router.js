import { lazy } from 'react';

export const ROUTES = [
  {
    path: '/login',
    component: lazy(() => import('../../Auth/Login')),
  },
  {
    path: '/register',
    component: lazy(() => import('../../Auth/Registration')),
  },
  {
    path: ['/', '/home'],
    component: lazy(() => import('../../Home/Home')),
  },
  {
    path: '/about',
    component: lazy(() => import('../../About/About')),
  },
  {
    path: '*',
    component: lazy(() => import('../../Exception/NotFound')),
  },
];
