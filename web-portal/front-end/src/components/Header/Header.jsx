import {
  AppBar,
  Box,
  Button,
  CssBaseline,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { Suspense, useEffect, useState } from 'react';
import { BrowserRouter as Router, NavLink, Redirect, Route, Switch } from 'react-router-dom';
import AuthService from '../../services/AuthService.js';
import theme from '../../theme/Theme.js';
import About from '../About/About.jsx';
import Login from '../Auth/Login.jsx';
import Registration from '../Auth/Registration.jsx';
import { LoadingSpinner } from '../Container/LoadingSpinner';
import Home from '../Home/Home.jsx';
import { NavLinks } from '../Navigation/Navlinks';

const primary = theme.palette.primary;
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  breakpoints: { ...theme.breakpoints.values },

  appBar: {
    height: '4.15rem',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  toolbar: {
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(0, 5),
  },

  typography: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontWeight: 400,
    fontSize: `1rem`,
    letterSpacing: `0.02rem`,
    textTransform: `uppercase`,
    padding: `1.2rem 0`,
    cursor: `pointer`,
  },

  navlinks: {
    display: 'flex',
    justifyContent: 'center',
    flexGrow: 1,
  },

  navDisplayFlex: {
    display: `flex`,
    color: '#FFF',
    justifyContent: `space-between`,
    padding: theme.spacing(0),
  },

  navItems: {
    textDecoration: `none`,
    textTransform: `uppercase`,
    [theme.breakpoints.up('md')]: {
      margin: '0 5px',
    },

    '&.active': {
      backgroundColor: primary.dark,
      borderBottom: '3px solid #FFF',
    },

    '&:hover': {
      cursor: `pointer`,
      backgroundColor: primary.light,
    },
    '&:focus': {
      backgroundColor: primary.dark,
    },
  },

  linkItem: {
    display: `flex`,
    justifyContent: `space-between`,
    textDecoration: `none`,
    padding: theme.spacing(2, 1),
  },

  content: {
    display: 'block',
    flex: '1 1 auto',
    overflow: 'hidden',

    [theme.breakpoints.up('md')]: {
      margin: '4.25rem 0',
      padding: '1rem 2.5rem',
    },
    margin: '4.25rem 0',
    padding: `1rem 1.5rem`,
  },
}));

const Header = () => {
  const classes = useStyles();
  const navLinks = NavLinks;
  const [isLoading, setIsLoading] = useState(false);

  const [accountState, setAccountState] = useState(undefined);
  console.log('file: Header.jsx ~ line 120 ~ Header ~ accountState', accountState);

  const getAccountLocal = () => {
    setIsLoading(true);
    const userData = JSON.parse(localStorage.getItem('account'));
    if (userData) {
      setAccountState(userData[0]);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getAccountLocal();

    const setTimer = setTimeout(() => {
      getAccountLocal();
    }, 8000);

    return () => {
      console.log('Clear timeout');
      clearTimeout(setTimer);
    };
  }, []);

  const logOut = () => {
    AuthService.logout();
    <Redirect to="/login" />;
  };

  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar className={classes.appBar} elevation={1} position="fixed">
          <Toolbar className={classes.toolbar}>
            <Typography className={classes.typography} noWrap>
              <a style={{ textDecoration: 'none' }} href="/home">
                Web Portal
              </a>
            </Typography>
            <Box flexGrow={1} />

            {accountState ? (
              <>
                <div className="navlinks">
                  <List
                    component="nav"
                    aria-labelledby="main navigation"
                    className={classes.navDisplayFlex}
                  >
                    {navLinks.map((item, index) => (
                      <NavLink className={classes.navItems} exact={true} to={item.path} key={index}>
                        <ListItem button className={classes.linkItem} key={item.title}>
                          <ListItemText primary={item.title} />
                        </ListItem>
                      </NavLink>
                    ))}
                  </List>
                </div>
                <Box flexGrow={5} />

                <div className="mx-3">{<span>{accountState.username}</span>}</div>

                <Button variant="outlined" href="/login" onClick={logOut}>
                  Log out
                </Button>
              </>
            ) : (
              <div className="d-flex">
                {/* <Box flexGrow={5} /> */}
                <div className="m-x-5">
                  <Button variant="outlined" href="/login">
                    Login
                  </Button>
                </div>

                <div className="m-x-5">
                  <Button className="" variant="outlined" href="/register">
                    Register
                  </Button>
                </div>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </div>

      {/* ------------------------ */}
      <div className={classes.content}>
        <Suspense fallback={isLoading ? <LoadingSpinner /> : ''}>
          <Switch>
            <Route exact path={['/', '/home']} component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Registration} />
          </Switch>
        </Suspense>
      </div>
    </Router>
  );
};

export default Header;
