import { Link } from '@material-ui/core';
import React, { useContext, useRef, useState } from 'react';
import CheckButton from 'react-validation/build/button';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import { isEmail, isEmpty } from 'validator';
import AuthService from '../../services/AuthService';
import { AuthContext } from '../Contexts/AuthContext';
import './auth.scss';

const required = (value) => {
  if (isEmpty(value)) {
    return (
      <div className="alert alert-danger my-2" role="alert">
        <span>This field is required</span>
      </div>
    );
  }
};

const email = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger my-2" role="alert">
        <span>{value} is not a valid email.</span>
      </div>
    );
  }
};

const username = (value) => {
  if (value.length < 4 || value.length > 25) {
    return (
      <div className="alert alert-danger my-2" role="alert">
        <span> The username must be between 4 and 25 characters.</span>
      </div>
    );
  }
};

const initialnputResgisState = {
  email: '',
  username: '',
};

const Registration = () => {
  const { checkRegiters } = useContext(AuthContext);
  const form = useRef();
  const checkBtn = useRef();

  const [inputRegister, setInputRegister] = useState(initialnputResgisState);
  const [loading, setLoading] = useState(false);
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState('');

  const handleChangeinput = (e) => {
    const { value, name } = e.target;
    setInputRegister({
      ...inputRegister,
      [name]: value,
    });
  };

  const registration = () => {
    AuthService.register(inputRegister).then((res) => {
      setSuccessful(true);
      setLoading(false);
      setMessage('Register succesfull!');
    });
  };

  const handleRegistration = (e) => {
    e.preventDefault();
    setLoading(true);
    setSuccessful(false);

    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      setLoading(true);

      checkRegiters(inputRegister).then((res) => {
        if (res.code === 200) {
          registration();
        }
        const resMessage = 'Account is exists, please check your account!';
        setMessage(resMessage);
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  };

  return (
    <div className="form-register">
      <img
        className="profile-img-card"
        src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
        alt="reactImage"
      />
      <div className="title-register">
        <span>Registration via Email</span>
      </div>

      <Form onSubmit={handleRegistration} ref={form}>
        {!successful && (
          <div>
            <div className="form-group my-3">
              <label htmlFor="email">Email</label>
              <Input
                className="form-control"
                type="email"
                name="email"
                placeholder="name@example.com"
                value={inputRegister.email}
                validations={[required, email]}
                onChange={handleChangeinput}
              />
            </div>

            <div className="form-group my-3">
              <label htmlFor="username">Username</label>
              <Input
                className="form-control"
                type="text"
                name="username"
                placeholder="username"
                validations={[required, username]}
                value={inputRegister.username}
                onChange={handleChangeinput}
              />
            </div>

            <button
              className="btn btn-primary btn-block w-100 my-2"
              type="submit"
              disabled={loading}
            >
              {loading && <span className="spinner-border spinner-border-sm"></span>}
              <span>Registration</span>
            </button>
          </div>
        )}

        {message && (
          <div className="form-group">
            {successful === true ? (
              <div>
                <div className="alert alert-success" role="alert">
                  {message}
                </div>
                <div className="register">
                  <Link href="/login">Login again</Link>
                </div>
              </div>
            ) : (
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            )}
          </div>
        )}

        <CheckButton style={{ display: 'none' }} ref={checkBtn} />
      </Form>
    </div>
  );
};

export default Registration;
