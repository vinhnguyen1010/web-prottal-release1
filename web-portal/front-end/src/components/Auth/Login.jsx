import { Link } from '@material-ui/core';
import React, { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import CheckButton from 'react-validation/build/button';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import { isEmail, isEmpty } from 'validator';
import AuthService from '../../services/AuthService';
import './auth.scss';

const required = (value) => {
  if (isEmpty(value)) {
    return (
      <div className="alert alert-danger my-2" role="alert">
        <span>This field is required</span>
      </div>
    );
  }
};

const email = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger my-2" role="alert">
        <span>{value} is not a valid email.</span>
      </div>
    );
  }
};

const initialInputState = {
  email: '',
};

function Login() {
  const form = useRef();
  const checkBtn = useRef();
  const history = useHistory();

  const [inputEmail, setInputEmail] = useState(initialInputState);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');

  const handleChangeInputEmail = (e) => {
    const { value, name } = e.target;
    setInputEmail({
      ...inputEmail,
      [name]: value,
    });
  };

  const handleLogin = (e) => {
    e.preventDefault();

    setLoading(true);
    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      const param = { email: inputEmail.email };
      AuthService.login(param).then((res) => {
        if (res && res.data.length > 0) {
          // console.log(res);
          history.push(`/home`);
        }
        const resMessage = 'Email not exists';
        setMessage(resMessage);
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  };

  return (
    <div className="form-signin">
      <img
        className="profile-img-card"
        src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
        alt="reactImage"
      />
      <div className="title-login">
        <span>Login via Email</span>
      </div>

      <Form onSubmit={handleLogin} ref={form}>
        <div className="input-email my-2">
          <Input
            className="form-control my-2"
            name="email"
            placeholder="name@example.com"
            validations={[required, email]}
            value={inputEmail.email}
            onChange={handleChangeInputEmail}
          />
        </div>
        {message && (
          <div className="form-group">
            <div className="alert alert-danger" role="alert">
              {message}
            </div>
          </div>
        )}
        <div className="register">
          <Link href="/register"> Register Email</Link>
        </div>
        <button className="btn btn-primary btn-block w-100 my-3" type="submit" disabled={loading}>
          {loading && <span className="spinner-border spinner-border-sm"></span>}
          <span>Authentication</span>
        </button>

        <CheckButton style={{ display: 'none' }} ref={checkBtn} />
      </Form>
    </div>
  );
}

export default Login;
