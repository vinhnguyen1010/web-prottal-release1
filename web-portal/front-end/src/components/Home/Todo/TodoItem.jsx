import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React, { useContext, useState } from 'react';
import { LoadingSpinner } from '../../Container/LoadingSpinner';
import { ToastContext } from '../../Contexts/ToastContext';
import { TodoContext } from '../../Contexts/TodoContext';

const ItemTodo = (props) => {
  const { todo, onEditTodo, onDeleteTodo } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [messenger, setMessenger] = useState('');
  const [isCheckStatus, setIsCheckStatus] = useState(todo.status);
  const { editTodo } = useContext(TodoContext);
  const { showMessage } = useContext(ToastContext);

  const handleOpenDialog = (todo) => {
    setOpenDialog(true);
    const mess = 'Are you sure want to delete todo ID: ' + todo.id + '?';
    setMessenger(mess);
  };

  const handleCloseDialog = (e) => {
    setOpenDialog(false);
  };

  const onDeleteTodoClick = (todo) => {
    onDeleteTodo(todo);
    setOpenDialog(false);
  };

  const handleChangeStatusClick = (e) => {
    setIsCheckStatus(e.target.checked);
    const todoItem = {
      ...todo,
      [e.target.name]: !isCheckStatus,
    };
    setIsLoading(true);

    editTodo(todo.id, todoItem)
      .then((res) => {
        setIsLoading(false);
        showMessage('success', res.statusText + ' Updated Status successful');
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const dialogConfirm = (messenger) => {
    return (
      <Dialog maxWidth="xs" open={openDialog} onClose={handleCloseDialog} aria-labelledby="dialog">
        <DialogTitle id="dialog-title">{'Confirm delete Todo?'}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText>{messenger}</DialogContentText>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button onClick={handleCloseDialog} color="default">
            Cancel
          </Button>
          <Button autoFocus onClick={() => onDeleteTodoClick(todo)} color="primary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    );
  };

  return (
    <div>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <li
          className={
            todo.status ? 'task-item list-group-item-success' : 'task-item list-group-item-primary'
          }
        >
          <div className="d-flex justify-content-between">
            <Checkbox
              className="m-2"
              id="status"
              color="primary"
              name="status"
              value={isCheckStatus}
              checked={isCheckStatus}
              onChange={handleChangeStatusClick}
            />

            <div className="flex-grow-1 align-self-center">
              <span className={todo.status ? 'px-2 toggle-item' : 'px-2'}>{todo.description}</span>
            </div>

            <div className="align-self-center">
              <span className="icon-item">
                <IconButton aria-label="edit" onClick={() => onEditTodo(todo)}>
                  <EditIcon />
                </IconButton>
                <IconButton aria-label="delete" onClick={() => handleOpenDialog(todo)}>
                  <DeleteIcon />
                </IconButton>
                {dialogConfirm(messenger)}
              </span>
            </div>
          </div>
        </li>
      )}
    </div>
  );
};

export default ItemTodo;
