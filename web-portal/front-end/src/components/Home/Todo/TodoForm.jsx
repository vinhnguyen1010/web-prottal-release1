import { Button } from '@material-ui/core';
import React from 'react';

const AddTodo = ({ todoInput, onChangeTodoInput, onSubmitTodo }) => {
  return (
    <div>
      <h4>Create new Todo</h4>

      <form className="w-100" onSubmit={onSubmitTodo}>
        <div className="row m-t-10 pb-3">
          <div className="col">
            <input
              className="form-control"
              type="text"
              label="Description"
              placeholder="Description"
              name="description"
              autoComplete="off"
              autoFocus="on"
              maxLength={250}
              value={todoInput.description}
              onChange={onChangeTodoInput}
            />
          </div>
          <div className="col-1">
            <Button variant="contained" color="primary" onClick={onSubmitTodo}>
              Submit
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddTodo;
