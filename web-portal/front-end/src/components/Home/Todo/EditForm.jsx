import { Button } from '@material-ui/core';

const EditForm = ({ currentTodo, setIsEditing, onEditInputChange, onEditFormSubmit }) => {
  return (
    <>
      <h4>Updating Todo</h4>

      <form className="w-100" onSubmit={onEditFormSubmit}>
        <div className="row m-t-10 pb-3">
          <div className="col">
            <input
              className="form-control"
              autoFocus="on"
              label="Description"
              placeholder="Description"
              autoComplete="off"
              maxLength={250}
              name="description"
              value={currentTodo.description}
              onChange={onEditInputChange}
            />
          </div>

          <div className="col-2">
            <div className="d-flex">
              <div className="px-1">
                <Button
                  title="update"
                  variant="contained"
                  color="primary"
                  onClick={onEditFormSubmit}
                >
                  Update
                </Button>
              </div>

              <div className="px-1">
                <Button
                  title="cancel"
                  variant="contained"
                  onClick={() => setIsEditing(false)}
                  color="primary"
                >
                  Cancel
                </Button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default EditForm;
