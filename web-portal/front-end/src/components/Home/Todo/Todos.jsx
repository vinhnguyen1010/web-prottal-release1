import { Divider } from '@material-ui/core';
import React, { useContext, useEffect, useState } from 'react';
import { TodoContext } from '../../Contexts/TodoContext';
import EditForm from './EditForm';
import './todo.scss';
import TodoForm from './TodoForm';
import TodoItem from './TodoItem';
import { toast } from 'react-toastify';
import { ToastContext } from '../../Contexts/ToastContext';
import { LoadingSpinner } from '../../Container/LoadingSpinner';

toast.configure();
const Todos = ({ account }) => {
  const initialInputState = {
    accountId: account.id,
    description: '',
    status: false,
  };
  const [todoInput, setTodoInput] = useState(initialInputState);
  const [isEditing, setIsEditing] = useState(false);
  const [currentTodo, setCurrentTodo] = useState({});

  const { todos, createTodo, editTodo, fetchTodos, removeTodo } = useContext(TodoContext);
  const { showMessage } = useContext(ToastContext);
  const [isLoading, setIsLoading] = useState(false);

  const getRequestParams = (accountId) => {
    const params = { _sort: 'updatedAt', _order: 'desc' };
    if (accountId) {
      params['accountId'] = accountId;
    }
    return params;
  };

  const handleGetTodos = () => {
    const params = getRequestParams(account.id);
    setIsLoading(true);
    fetchTodos(params)
      .then((res) => {
        // console.log('get', res);
        setIsLoading(false);
        return res;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // get context for account and pass data account to
  useEffect(() => {
    handleGetTodos();

    return;
  }, []);

  // -------------------

  const handleChangeTodoInput = (e) => {
    const { value, name } = e.target;
    setTodoInput({
      ...todoInput,
      [name]: value,
    });
  };

  const handleSubmitTodo = (e) => {
    e.preventDefault();
    if (!initialInputState) return;
    setIsLoading(true);
    createTodo(todoInput)
      .then((res) => {
        console.log(res);
        // showMessage('success', 'Create successful');
        showMessage('success', res.statusText + ' successful');
        setTodoInput(initialInputState);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // -------------------
  const handleEditInputChange = (e) => {
    setCurrentTodo({
      ...currentTodo,
      [e.target.name]: e.target.value,
    });
  };

  const handleEditFormSubmit = (e) => {
    console.log('todo binding on form edit', currentTodo);
    e.preventDefault();
    setIsLoading(true);
    editTodo(currentTodo.id, currentTodo)
      .then((res) => {
        console.log('handle edit', res);
        setIsLoading(false);
        setIsEditing(false);
        showMessage('success', res.statusText + ' Updated successful');
        setTodoInput(todoInput);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleOnEditTodo = (todoInput) => {
    console.log(' todo selected', todoInput);
    setIsEditing(true);
    setCurrentTodo({ ...todoInput });
  };

  const handleDeleteTodo = (todo) => {
    console.log('handleDeleteTodo ~ id', todo.id);
    setIsLoading(true);
    removeTodo(todo)
      .then((res) => {
        console.log('handle delete', res);
        showMessage('success', res.statusText + ' Deleted successful');
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <div className="row">
          {isEditing === true ? (
            <div className="row">
              <EditForm
                currentTodo={currentTodo}
                setIsEditing={setIsEditing}
                onEditInputChange={handleEditInputChange}
                onEditFormSubmit={handleEditFormSubmit}
              />
            </div>
          ) : (
            <div className="row">
              <TodoForm
                todoInput={todoInput}
                onChangeTodoInput={handleChangeTodoInput}
                onSubmitTodo={handleSubmitTodo}
              />
            </div>
          )}

          {/*  */}
          <h4>Todo List</h4>
          <Divider />

          {todos.length > 0 ? (
            <div>
              <ul className="list-group list-item-task">
                {todos.map((todo, index) => (
                  <TodoItem
                    todo={todo}
                    key={index}
                    onEditTodo={handleOnEditTodo}
                    onDeleteTodo={handleDeleteTodo}
                  />
                ))}
              </ul>
            </div>
          ) : (
            <div className="no-data">
              <h3> No data Todo </h3>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Todos;
