import React, { useEffect, useState } from 'react';
import { ToastContextProvider } from '../Contexts/ToastContext.js';
import { TodoContextProvider } from '../Contexts/TodoContext.js';
import Todos from './Todo/Todos';

const Home = () => {
  const [currentAccount, setCurrentAccount] = useState(undefined);

  useEffect(() => {
    const authHeader = () => {
      const data = JSON.parse(localStorage.getItem('account'));
      if (data) setCurrentAccount(data[0]);
    };

    authHeader();
  }, []);

  return (
    <div className="container col">
      {currentAccount ? (
        <TodoContextProvider>
          <ToastContextProvider>
            <Todos account={currentAccount} />
          </ToastContextProvider>
        </TodoContextProvider>
      ) : (
        <div>
          <p style={{ textAlign: 'center' }}>Not authorised</p>
        </div>
      )}
    </div>
  );
};

export default Home;
