import TodoService from '../../services/TodoService';
import { ADD_TODO, DELETE_TODO, GET_TODOS, UPDATE_TODO } from '../index';

export const addTodo =
  ({ description, status }) =>
  async (dispatch) => {
    try {
      const res = await TodoService.addTodo({ description, status });
      dispatch({
        type: ADD_TODO,
        payload: res,
      });
      return Promise.resolve(res);
    } catch (error) {
      return Promise.reject(error);
    }
  };

export const updateTodo = (id, todo) => async (dispatch) => {
  try {
    const res = await TodoService.updateTodo(id, todo);
    dispatch({
      type: UPDATE_TODO,
      payload: res,
    });
    return Promise.resolve(res);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteTodo = (id) => async (dispatch) => {
  try {
    const res = await TodoService.deleteTodo(id);
    dispatch({
      type: DELETE_TODO,
      payload: res,
    });
    return Promise.resolve(res);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getTodos = () => async (dispatch) => {
  try {
    const res = await TodoService.getTodos();
    dispatch({
      type: GET_TODOS,
      payload: res,
    });
    return Promise.resolve(res);
  } catch (error) {
    return Promise.reject(error);
  }
};
