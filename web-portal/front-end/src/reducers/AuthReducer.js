import AuthService from '../services/AuthService';
import { AUTH_LOGIN } from './index';

export const authReducers = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case AUTH_LOGIN:
      return state;

    default:
      return state;
  }
};
