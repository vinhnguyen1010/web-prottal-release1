import { GET_TODOS, ADD_TODO, UPDATE_TODO, DELETE_TODO } from './index';

export const todoReducers = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_TODO:
      return [...state, payload];

    case GET_TODOS:
      return payload;

    case UPDATE_TODO:
      return state.map((todo) => {
        if (todo.id === payload.id) {
          return payload;
        }
        return todo;
      });

    case DELETE_TODO:
      return state.filter((todo) => todo.id !== payload.id);

    default:
      return state;
  }
};
