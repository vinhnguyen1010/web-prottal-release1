import { CssBaseline, ThemeProvider } from '@material-ui/core';
import './App.scss';
import Header from './components/Header/Header';
import theme from './theme/Theme';
import './styles/styles.scss';
import { AuthContextProvider } from './components/Contexts/AuthContext';
function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AuthContextProvider>
        <Header />
      </AuthContextProvider>
    </ThemeProvider>
  );
}

export default App;
