// import { createMuiTheme } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';

const themeDefault = createTheme({
  palette: {
    primary: {
      light: '#ffa199',
      main: '#ef5350',
      dark: '#ac4841',
      contrastText: '#FFF',
    },

    dark: {
      light: '#808080',
      main: '#616161',
      dark: '#434343',
      contrastText: '#FFF',
    },

    background: {
      paper: '#ffffff',
      default: '#fafafa',
    },

    warning: {
      main: '#ff9800',
    },

    error: {
      main: '#f44336',
    },

    info: {
      main: '#2196f3',
    },

    success: {
      main: '#4caf50',
    },

    overrides: {
      MuiButton: {
        root: {
          fontSize: '1.5rem',
          fontWeight: 400,
        },
      },
    },
  },
});

export default themeDefault;
