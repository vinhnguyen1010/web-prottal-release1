import axios from 'axios';

export const BASE_URL = 'http://localhost:8000/api';

export default axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const HttpMethod = {
  POST: 'post',
  GET: 'get',
  PATCH: 'patch',
  DELETE: 'delete',
};

const makeRequest = async (url, method = 'get', data, requestType = 'json') => {
  const requestURL = BASE_URL + url;
  console.log('requestURL===', requestURL);

  const response = await axios({
    method: method,
    url: requestURL,
    headers: requestType,
    body: data
  });

  return response;
};

export const sendGetRequest = async (url, reqData) => {
  const response = await makeRequest(url, HttpMethod.GET, reqData);
  console.log('response==', response)
  return response;
};


export const sendPostRequest = async (url, reqData) => {
  const response = await makeRequest(url, HttpMethod.POST, reqData);
  console.log('response==', response)
  return response;
}

export const sendPatchRequest = async (url, reqData) => {
  const response = await makeRequest(url, HttpMethod.PATCH, reqData);
  console.log('response==', response)
  return response;
}

export const sendDeleteRequest = async (url, reqData) => {
  const response = await makeRequest(url, HttpMethod.DELETE, reqData);
  console.log('response==', response)
  return response;
}