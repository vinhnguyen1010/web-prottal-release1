const faker = require('faker');
const fs = require('fs');

// Random todo Id
const randomID = () => {
  return Math.floor(Math.random(1) * 100000) + 99999;
};

// Random birth of day
// const randomBirthday = () => {
//   const start = new Date(1985, 1, 1);
//   const end = new Date(2000, 1, 1);
//   return new Date(
//     start.getTime() + Math.random() * (end.getTime() - start.getTime())
//   ).toLocaleDateString();
// };

// Ramsom users account information
const randomAccountList = (n) => {
  if (n < 0) return [];
  const accountDefault = {
    id: '123456',
    email: 'admin@gmail.com',
    username: 'Admin',
    password: 'Admin1234',
    createdAt: new Date().getTime(),
    updatedAt: new Date().getTime(),
  };
  const accountList = [accountDefault];
  Array.from(new Array(n)).forEach(() => {
    const user = {
      id: randomID().toString(),
      email: faker.internet.email(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      createdAt: new Date().getTime(),
      updatedAt: new Date().getTime(),
    };
    accountList.push(user);
  });
  return accountList;
};

// Random todos information
const randomTodoList = (accountList, numberOfTodos) => {
  if (numberOfTodos < 0) return [];
  const todoList = [];
  for (const account of accountList) {
    Array.from(new Array(numberOfTodos)).forEach(() => {
      const todo = {
        accountId: account.id,
        id: randomID().toString(),
        description: faker.commerce.productDescription(),
        status: faker.random.boolean(),
        createdAt: new Date().getTime(),
        updatedAt: new Date().getTime(),
      };
      todoList.push(todo);
    });
  }
  return todoList;
};

// Random todos
(() => {
  const accountList = randomAccountList(5);
  const todoList = randomTodoList(accountList, 5);

  const db = {
    accounts: accountList,
    todos: todoList,
    profile: {
      name: 'vinh nguyen',
    },
  };

  // Write db object to db.json
  fs.writeFile('./src/db.json', JSON.stringify(db), () => {
    console.log('Generate data successfully.');
  });
})();
