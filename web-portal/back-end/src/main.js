const jsonServer = require('json-server');
const queryString = require('query-string');
const server = jsonServer.create();
const router = jsonServer.router('src/db.json');
const middlewares = jsonServer.defaults();

const PORT = process.env.PORT || 8000;

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);

// Add custom routes before JSON server router
server.get('/echo', (req, res) => {
  res.jsonp(req.query);
});

// To handle POST. PUT, DELETE and PATCH we need to use a body-parser
// We can use the one used by JSON Serer

server.use(jsonServer.bodyParser);

server.use((req, res, next) => {
  switch (req.method) {
    case 'POST': {
      req.body.createdAt = Date.now();
      req.body.updatedAt = Date.now();
    }

    case 'PATCH': {
      req.body.updatedAt = Date.now();
    }

    case 'DELETE': {
      res.status(200);
    }
  }

  // Continue to JSON Server router
  next();
});

// Count total pages
const countPages = (limit, rows) => {
  if (rows % limit > 0) {
    return Math.floor(rows / limit) + 1;
  }
  return Math.floor(rows / limit);
};

// Custom output for list with pagination
router.render = (req, res) => {
  // Check GET with pagination
  // If yes with custum output
  const headers = res.getHeaders();
  const totalCountHeader = headers['x-total-count'];
  if (req.method === 'GET' && totalCountHeader) {
    const queryParams = queryString.parse(req._parsedUrl.query);
    const result = {
      data: res.locals.data,
      pagination: {
        _page: Number.parseInt(queryParams._page) || 1,
        _limit: Number.parseInt(queryParams._limit) || 10,
        _totalRows: Number.parseInt(totalCountHeader),
        _totalPages: countPages(
          Number.parseInt(queryParams._limit),
          Number.parseInt(totalCountHeader)
        ),
      },
    };
    return res.jsonp(result);
  }
  // Otherwise, keep default behavior
  res.jsonp(res.locals.data);
};

// Use default router
server.use('/api', router);
server.listen(PORT, () => {
  console.log('JSON Server is running ', PORT);
});
